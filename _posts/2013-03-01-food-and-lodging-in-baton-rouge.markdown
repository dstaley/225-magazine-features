---
title: Food and Lodging in Baton Rouge
subtitle: A not-so-savage journey into the heart of the American Dream
author: Chris Frink
image: "images/225gonzosteadmanillojoytaney.jpg"
date:   2013-03-01 12:42:43
layout: post
custom_css: true
---
We were halfway down the River Road, the levee looming green on our right, when it seemed like the drugs were wearing off. Then I saw the writhing knot of giant bats rise up over the trees off to the left.

We were headed into the heart of the American Dream, toward [L’Auberge Casino and Hotel](https://www.lbatonrouge.com/), the city’s newest temple of food and drink, a palace promising luxury and service and—most importantly–around-the-clock gambling. (Excuse me, “gaming.” We don’t have “gambling” in Louisiana.) 

“Bats? What the hell are you talking about, bats?” muttered my veterinarian. 

That was nothing but a cloud of black smoke from the LSU firefighter training center, The Veterinarian pointed out. “You fool. Your eyes are still dilated. That’s why I’m driving. You can’t see a thing,” she said.

I pulled down the visor and looked in the mirror. My pupils were as big as dimes. I looked like a well-dressed drug fiend, like a character who’d staggered out of a Hunter S. Thompson book. In reality, I’d been to the eye doctor that morning, and the eye drops had yet to wear off.  

Yes, I had a satchel full of drugs in the back, but these pills are legal: something for cholesterol, a blood-sugar downer, baby aspirin to thin the blood, a rainbow of flavored antacids and a mellow muscle relaxer. My back’s been bothering me. The only grass was on the levee, and I was counting on the casino to sell the liquor.

Not long after the “bats” dissipated, the shiny new L’Auberge tower loomed into view. I was late for lunch. Sometimes, The Veterinarian drives slower than a granny on the way to a funeral, even when harassed by hallucinated bats.

<figure>
  <img src="{{ site.base_url }}images/225L'AubergeStock_001_COLOR.vu.jpeg">
  <figcaption>L’Auberge features three restaurants, a PJ’s Café, a rooftop pool and more.</figcaption>
</figure>

This whole odyssey, the grueling 10-mile journey, started with an email in the weeks before Isaac. 

“Frink,” The Editor wrote from the control room in the center of the corporate bunker out on Jefferson Highway, “There’s a casino opening up. Go cover it and make sure you get there before the new-casino smell wears off.”

The Editor and his Thugs forced me to pick up my fee in advance, you know, for the gaming. But no, they said, I could not expense out the rental of a large red convertible for the weekend. “The trip’s only 10 miles, you fool.”

So, there I was 10 miles later dropped unceremoniously at L’Auberge by The Veterinarian, who claimed she needed to get back to work. “Somebody has to earn a living, you fool.” 

The casino-hotel straddles the levee, is buffered by minty-fresh landscaping and rises above scrubby pastureland and the newly planted orderliness of subdivisions. The 12-story hotel sits on the land side of the levee, and the casino-restaurant-bar-theatre complex on the batture.  Between is a six-lane driveway/valet parking facility as bustling and well organized as a taxiway at a mid-market airport.   

After staff members guided me through a bout of middle-aged confusion, I was in Steak18, L’Auberge’s signature restaurant, in the midst of the suited dignity of casino executives with a few scruffy journalist-types here and there. (The “18” of 18Steak is a nod to Louisiana being the 18th state in the union and is a theme picked up in the menu, from salad to dessert.)

I steeled myself to maintain my composure among these professionals. Then it dawned on me. My vision was blurry, and I was hiding behind Ray-Bans, because I had come straight from the ophthalmologist. “You’re not a degenerate drug fiend, you fool.” 

Despite my meanderings, I wasn’t late. Platters of local charcuterie appeared moments after I was seated. We were offered a stunning array of cold sliced boudin, alligator sausage, hogshead cheese and other delights, paired with piles of pickled goodies. Who knew one could pickle celery and that sweet crunchiness combined so deliciously with ’gator sausage?

That first taste—a pre-appetizer appetizer—should have been a hint. In time it became clear: these fiends were out to kill me. To force me to eat salad after salad, appetizer after appetizer.  Caesar with fried oysters and perfect leaves of romaine. Little lobster egg rolls. A bowl piled with BBQ shrimp … lurking down in the sauce, lobster dumplings. 

They just kept coming. Oh, the humanity. Oh, the depravity. “Oh, pass the bread.”

<figure class="steak">
  <img src="{{ site.base_url }}images/225LambRibeyeL'Auberge_005_COLOR.cvb.jpeg">
  <figcaption>Lamb ribeye at 18 Steak.</figcaption>
</figure>

I started with a nice merlot then a switched to a crisp pinot grigio. Back to the merlot when the petit filet arrived. Oh, the belt-loosening joy. I thought I’d survived, but then these smiling assassins, these beasts with impeccable serving skills, delivered the coup de grace: dessert. 

Not just dessert, but “Chocolate 18 Ways.” 

I’m a dead man, I thought, as I stared at the slim tray in front of me. (Easy to maintain my cool behind the Ray-Bans.) The textures run from liquid to airy and crunch to crumble and just-plain-smooth. I’m dedicated and—I’ve been told—a fool.

I ate it all. From “Zenith White Chocolate Mousse Shooter” to “Atomized 64% Guayaquil Chocolate Dust” and “Grue Nougatine” to “Hazelnut Gianduja Chocolate Cream,” I took one (18, really) for the team. Every bite different but each—at heart—chocolate. Every one delicious.

I survived and scurried off before the smiling assassins took notice. 

My head screamed for a nap, but my professionalism and the fear of The Editor and his goons forced me into the casino for gaming lessons. In the High Rollers Salon I got a seminar in intermediate blackjack. Increase your bet after you win; that way you’re playing with the house’s money. Whether you chose to hit or stay on 16—that most borderline of blackjack numbers— stick with one strategy. You don’t automatically split aces; it depends on what the dealer’s showing.

In the High Rollers Salon, I saw some actual High Rollers actually gaming with chips bought with actual money. (Gamecocks fans in town for the LSU game. They looked as regular as South Carolina fans can: polos, slacks, loafers. They even wore socks. On their feet.) These guys were up to $1,000 per hand. Serious mojo going on.

Can we watch, I asked? My smooth and gracious host/instructor demurred; guests are in the Salon because they’ve earned some privacy and deference. He was too polite to say, “If you were betting a thousand dollars a hand, would you want some ruffian ‘journalist’ looming over you and your friends? You fool.”

On to roulette. That’s a straight-up game that even I could understand. The odds were printed out right there on the table.

Craps. That’s what I wanted to learn. I wanted to SHOOT craps. That’s a game with a real verb, I thought. Everything else you “play.” This game has action. It has dice to hold and throw. Three minutes into my education, and I was in far over my head. The assassins’ beef and chocolate had clogged my limited faculties. The odds, the variety of bets, the permutations. At five minutes, my brain hurt. Must escape. Must nap.

One hitch in the silky L’Auberge hospitality machine: my room was not ready. A short wait at the rooftop bar. Sweeping views and local beers. I even met some South Carolina fans. They were polite, spoke in complete sentences and appreciated being tipped to the local brew, Tin Roof’s Blonde and Perfect Tin.

Soon, I discovered the cause of the delay. 225 had arranged for a suite.

Something like 850 square feet of luxury in a living/dining room, bedroom and bathroom so massive it had its own lobby. There was a glassed-in shower wide enough to hold the average conference on a major league pitcher’s mound and a bathtub big enough to float a mid-sized bass boat. And—get this—a TV embedded behind the mirror. Watch the weather and shave at the same time.

I could get used to this.

Later, after the Veterinarian rejoined the party and we hit the buffet (seemed like a hundred yards of goodness; the gelato was a revelation) and dropped in on the Darius Rucker concert (stunning outdoor venue; we left during an off-key “Purple Rain”). 

Later, long past the Veterinarian’s bedtime, fueled by Red Bull and haunted by The Editor’s orders, I saddled up and headed down to the casino. It was still packed, even an hour after the concert ended. I was eager to test-drive my expanded blackjack skills, but the low-bet tables were full. The craps tables were hopping, but … I headed to the poker room. Texas Hold ’Em played against other players, not against the house. (Don’t fret about the house; they get their cut.)

I looked like fresh fish to the guys hiding behind towers of chips. The rhythm eluded me at first. I frittered away my Lilliputian pile of chips in a methodical manner. The sharks eyed me. At one point, I may have tossed away a winning hand. Then I bounced back, pulling in a pot on a middling straight I didn’t even realize I had.

I caught the rhythm, the ebb and flow. I got a feel for the men at the table. Some were serious, some casual, some just drunk. Playing conservative and remembering a lesson from my blackjack seminar, sticking with that one strategy, I stayed afloat with a couple of modest pots. The drunks didn’t stay afloat long. They left the game. Their money didn’t.

Finally, just before 4, I started with a pair of kings. As the cards flopped onto the felt that pair became two pair then a full house. One of the sharks, fat from feeding on drunks, pounced but held only a straight. That pot was a mouthful for me, but to him, with something like $1,500 stacked in front of him, it was a nibble.

I remembered the most important lesson any successful gambler knows: quit while you’re ahead. I looked around the table. I cashed in (doubling my money) and headed for the suite to cash myself in.

Sweet sleep. A little room service breakfast and back to the real world. I drove us home. No bats.