---
title: Thinking for a Change
subtitle: The rise of education alternatives in Baton Rouge
author: Lauren Brown
image: "images/thinking-for-a-change.jpg"
date:   2013-04-01 12:42:43
custom_css: true
layout: post
---
If you fall asleep in class, your teacher might swat you with a ruler. Or give you detention. Or even pull a prank on you. Nancy Zito turns off the lights.

The Director of the [Gardere Community Christian School](http://gardereschool.com/) understands that she needs to meet her students’ physical needs before she can meet their educational needs.

One of her students’ needs is sleep.

“We have families living with maybe eight people in a two-bedroom apartment,” Zito says. In at least one of those families, the children sleep on the floor in the living room. “Their brothers stay up late watching TV, so they’re up late. We give them 15 minutes to nap in the morning, and they’re fine.”

Zito says the nap times, hot breakfasts and lunches, and the individual attention she and her faculty show their 30 students are a few of the reasons they’ve been successful in bringing the students’ performance up several grade levels since transitioning from public schools. 

The Gardere area is only one of the many local communities with poor-performing public schools. In 2012, [the state gave 62% of public schools in East Baton Rouge Parish a grade of D or F](http://www.businessreport.com/daily-report/AM/1302013/EBR_public_schools_show_improvement_on_BRAC_report_card). Another 26% received a C. Those grades don’t lie. Education in Baton Rouge needs to change.

<figure>
  <img src="{{ site.base_url }}images/zito.jpg">
  <figcaption>Nancy Zito founded Gardere Community Christian School to offer a faith-based and mentor-focused education to children in the Gardere community.</figcaption>
</figure>

Zito is intent on making that change. While her school is a private elementary, she focuses her efforts on students from the Gardere neighborhood who struggled in their previous educational situations. The school, which began as a tutoring program, accepted its first batch of students last school year.

“We didn’t get students who were doing extremely well in public school,” she says. “All of our students came in last year at a first-grade or below level, and we had first- through fourth-graders [last year].”

<q class="left blue">"He was going to be put in special needs because when they can’t do anything, that’s what they do." <small>NANCY ZITO, founder, Gardere Community Christian School</small></q>

One student now in the school’s first fifth-grade class, was at a Pre-K level when he arrived a year ago. Since then, his reading level has advanced to third grade, and his math to the fifth-grade average.

“He was going to be put in special needs because when they can’t do anything, that’s what they do,” Zito says. “Get them put on medication or put them in special needs classes.”

Public schools, with large classes and governmental pressure dictating lesson plans, rarely have the luxury of individualized attention like this child received from Zito’s faculty. The former New Yorker says Baton Rouge has some great public schools, but individual attention becomes difficult in large classes, and the school’s focus becomes security and discipline rather than academics. 

As for that boy now catching up with his peers nationally, “He’s not special needs,” Zito says.

“He’s fantastic.”

In Baton Rouge, Zito’s private school is not the only new and alternative effort to work at improving education.

“It’s not surprising [to see more education alternatives],” says Chas Roemer, the Baton Rouge-based president of the state’s Board of Elementary and Secondary Education. “What is surprising is the amount of time it has taken for these efforts to really come to the front given the enormous need in our community. Change is difficult, though, and always seems like swimming upstream.”

For his part, Parish Superintendent Bernard Taylor is thinking differently, too. He wants to create attendance regions so parents have options for where their children go to school, instead of feeling trapped in low-performing neighborhood schools.

The East Baton Rouge Parish School System is working with the Recovery School District, a special school district administered by the Louisiana Department of Education, to create an Achievement Zone to completely change Baton Rouge’s lowest-performing schools.

Called New Schools for Baton Rouge, the project is raising $30 million to turn around schools in North Baton Rouge through one educationally radical idea: autonomy.

This Baton Rouge Area Foundation-funded start-up is based on [New Schools for New Orleans](http://www.newschoolsforneworleans.org/), an effort that, in the aftermath of Hurricane Katrina, transformed the Crescent City’s public education model with a dramatic boost to performance scores and graduation rates.

“Yes, the hurricane was a catalyst, but any community in the state or country doesn’t need a hurricane to make real change,” says Chris Meyer, CEO of New Schools for Baton Rouge. “If we prove it’s possible here, in now our most challenged neighborhood, we’re not only showing this proof that it can happen with anyone, we’re also showing that New Orleans wasn’t a fluke.” 

<figure>
  <img src="{{ site.base_url }}images/meyer.jpg">
  <figcaption>Chris Meyer, CEO of New Schools for Baton Rouge, is leading an effort to improve up to 25 public schools in the area.</figcaption>
</figure>

If all goes according to plan, that’s exactly what he’ll prove. Meyer is scouring the country for educators and school leaders who have shown their capabilities in the past. He expects to have a complete list of applicants by May 1. New Schools will only be working with schools that don’t follow school board orders, such as charter schools, so those hired can do what they do best in the best way they do it. 

“We want to invest in school models or school operators that have already demonstrated success,” Meyer says. “They’ve got be given maximum autonomy, so no one from a central office should dictate how they operate day-to-day.” This autonomy isn’t available for most traditional public schools that receive their orders from the school board. 

“We should set a bar of accountability really high for the school and say we expect that you’re going to deliver all your students on a path to college and career-readiness,” he continues. “But we’re not going to tell you how to do it, because we’ve seen your track record of success. We know you’ve done it before, so we expect you to do it again.”

Meyer plans for New Schools to keep a watchful eye over the changes and to weed and prune as circumstances dictate. “If you have accountability on those schools, if they’re not able to get the job done, there’s a way to take them out and put in somebody else who can get the job done.”

The “new” in the movement’s title refers to the newfound power schools must have at the educator level in order for change to take place. Meyer, a former [Teach for America](http://www.teachforamerica.org/) instructor, says he saw firsthand the educational disaster that happens when schools cannot make their own decisions. 

“In my classroom,” he says, “I had high expectations for my kids in their behavior and their learning, and things they would deliver to keep pushing themselves to hit the bar. Yet I’d look in the classroom next to me, and the teacher would be asleep with her feet on the desk.”

In this first year of teaching in New Orleans before Katrina, he saw four principals come and go at his school.

“None of those principals could fire me; none of those principals could have moved me anywhere else. I wasn’t ever picked to be on their team by them. I was put there by a central office,” he says. “I got to see, both on the school level and the system level, a dysfunctional system that wasn’t actually working for kids.”

<q class="right green">"I’d look in the classroom next to me, and the teacher would be asleep with her feet on the desk." <small>CHRIS MEYER, CEO, New Schools for Baton Rouge</small></q>

Meyer says the New Orleans school system was set up in a way that protected jobs rather than educated children.

“The number of people you would hear [making] comments [about] kids as young as elementary and kind of throwing them to the waste bin and saying, ‘There’s nothing we could do for them’—it was just abysmal,” he says.

Nothing we could do for them. This defeatist attitude, the one also being challenged by Zito, is not unrecognizable in Baton Rouge. However, after the recent improvements in New Orleans, more Baton Rougeans have been inspired to reevaluate our public school system.

For Meyer, this reevaluation begins with the goal of reaching 12,000 students in North Baton Rouge in the next five years.

“People will say the reason those schools are failing is because of things like poverty,” Meyer says. “I fundamentally disagree with that. Poverty isn’t the destiny of a school.”

According to Meyer, that destiny lies in the hands of those making the decisions. Who is hiring and firing teachers? It needs to be someone who sees their performance on a daily, not yearly, basis. Who decides what students will learn? It should be teachers who know their students’ needs, not someone writing a blanket memo for an entire parish.

“When you see an excellent school,” Meyer says, “you see some excellent things happening in those schools.” (See sidebar for Meyer’s checklist for success.)

The general agreement among many working for change seems to be that this autonomy, this power, is most easily found in charter schools. Charter schools are public schools funded by local, state and federal tax dollars, just like any other public school, though these innovative schools receive less funding in exchange for more flexibility. Attendance is by choice, not mandated by geography.

“We believe that, regardless of socio-economic status, our students will have access to an excellent education,” says Cheryl Ollmann, principal of [Baton Rouge Children’s Charter](http://www.brchildrenscharter.org/). 

Once a D- school, Children’s Charter is both the oldest and, recently, most successful charter school in Baton Rouge.

“Given leadership changes, the autonomy allowed as a charter, and our business partners, in 2011 we were the fifth highest improved school in the parish,” wrote former Principal Marc Commanducci in a recent email to supporters.

<q class="left yellow">"Regardless of socio-economic status, our students will have access to an excellent education." <small>CHERYL OLLMANN, principal of Baton Rouge Children’s Charter</small></q>

These “education warriors” agree that having the power to cater to students’ individual needs is an essential ingredient for educational success. [Louisiana Connections Academy](http://www.connectionsacademy.com/louisiana-school/home.aspx) is taking that concept to a new level.

Connections Academy is a national online school organization that provides teachers with a curriculum. The teachers then adjust those lessons as needed for their virtual students. 

“All teachers are fully aware of which students need accommodations, as well as [which] students need enrichment lessons,” says Shari Laterrade, a local Connections teacher.

<figure>
  <img src="{{ site.base_url }}images/laterrade.jpg">
  <figcaption>Shari Laterrade is a local online instructor with Connections Academy.</figcaption>
</figure>

The online K-12 public school is a good option for students in the entertainment industry who need flexible schedules, as well as students who were bullied and need healthy educational environments, she says.

“Students can progress through lessons at their own pace while having access to caring and certified teachers,” Laterrade says. “As a middle school English and Language Arts teacher, I ‘meet’ with my students every week in my ‘Live Lesson’ classroom. I can address individual student concerns in real time.”

The program uses webcams, headsets and special software to ensure students get face-to-virtual-face interaction and customized lessons. When more socialization and hands-on lessons are needed, the students take field trips.

“Baton Rouge’s goal should be to be the nation’s leader in the idea that public education means more than just government-run schools,” Roemer says. “We need to change the phrase ‘public education’ to ‘educating the public.’ The trend will be and should be that we will be a community that embraces educational excellence, and the source of that education will be across multiple providers, rather just a traditional system.”

Today, those multiple providers are becoming a reality. Baton Rouge has Zito reaching underserved Gardere children, Meyer remodeling as many as 25 North Baton Rouge schools in five years, and Laterrade offering online education. In total, seven charter schools are operating in Baton Rouge. [Thrive](http://thrivebr.org/), a chartered boarding school, is completing its first year. Of course, educators in public and private schools across the parish are striving now more than ever to help students reach their maximum potential.

Turn on the lights. Baton Rouge is waking up.