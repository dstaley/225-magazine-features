---
title: The block rockin’ beat
subtitle: Better Block BR will envision a new Government Street
author: Amy Alexander
youtube: "AS5-5VeuLFU"
date:   2013-04-01
layout: post
---
You rumble across railroad tracks and potholes from downtown in the left lane. You see a car at a full stop ahead of you, beneath the tilted telephone poles, prepping to turn left. You slam the brakes. Screech. Cuss. Speed up. Look forward.

This is Government Street.

And this is also Government Street: A road of character, with good bones, on an honest to God grid with sidewalks and a rich history beside beautiful neighborhoods.

The road is, in a word, complicated. For the past several years, nearby businesses and homeowners have yearned to revive this blighted traffic hazard. Government still has what it takes to be a great road, planning experts say.

“It’s been a priority area for the urban renewal district,” says Camille Manning Broome, Director of Planning for the [Center For Planning Excellence (CPEX)](http://cpex.org/).

It could become a thriving district of arts, business and entertainment. It could be a vibrant link between downtown and some of Baton Rouge’s most storied residential areas.

It’s a matter of citizens and city applying physical, financial and bureaucratic elbow grease to update ordinances and shift the infrastructure of the area. Because Government is a state highway, the city will have to coordinate its efforts with the state of Louisiana to make changes.

<figure>
  <img src="{{ site.base_url }}images/cpex.jpg">
  <figcaption>Center for Planning Excellence staff members, from left, John Carpenter, Rachel Diresto, Elizabeth “Boo” Thomas, Camille Manning Broome and Phillip LaFargue are the team bringing the Better Block concept to Baton Rouge this month.</figcaption>
</figure>

To inspire the hard work and cooperation it’ll take to make it real, you have to have a big, convincing vision.

Enter [Better Block BR](https://www.facebook.com/BetterBlockBr), a pop-up model of what Government could become.

On April 13-14, Baton Rouge will see a Government Street of dreams come to life. The two blocks from the Bedford to Beverly will become a little bit like Atlanta’s [Little Five Points](http://www.atlanta-midtown.com/business/l5p/), New Orleans’ [Magazine Street](http://www.magazinestreet.com/) or, more lofty, the [Champs Elysees](http://www.champselysees-paris.com/).

For two days, Better Block BR will show us what it’s like when a Baton Rouge street works for people in cars and on foot. Highlights will include a bike path, street trees, an information kiosk, street side parking, road-hugging storefronts, modern bus stops and signage and crosswalks, community gathering spaces, a nature trail, a coffee shop and a live example of complete streets concepts. 

It’s a combined effort by several government and civic groups who believe that if you build it, people will come and get behind making the dream come true.

It’s been an idea in some people’s minds for decades.

Back in 2003, Mary Ann Caffery told a gathering of reporters about the rich history of Government as part of the Government Street corridor project and her hopes for a great future.

“The state of Government Street was something that had been addressed by Mid City Merchants since our founding in 1993,” Caffery said. “I have minutes from board meetings in which this was the only topic of the entire meeting.”

Better Block BR will show people what it’s like to slow down a little on this throughway that is typically a whirring mass of windshields. Some around Baton Rouge have questioned the impact that reducing the number of lanes on Government might have on traffic though.

<figure>
  <img src="{{ site.base_url }}images/bbbr.jpg">
  <figcaption>A rendering of the Government Street stretch between Bedford Drive and Beverly Drive that will host the Better Block BR event April 13-14.</figcaption>
</figure>

The mayor’s assistant chief administrative officer, John Price, says this is a concentrated project with an emphasis on place and ideas, not an attempt to force cars off the road.

“Government Street is one of the primary roadways people travel on,” he says. The road sees in excess of 20,000 cars per day. The city isn’t planning on radically altering the entire length of Government Street—just a few blocks. And just for the weekend, to open the discussion for what could happen.

“If you can create a living charrette, it answers ‘plan fatigue,’” Price says.

Ready to honk your horn in anger? Simmer down, because traffic can slow a bit on an avenue here and there without huge traffic implications. And here’s an urban planning factoid: The best districts in the world don’t necessarily extend for mile after mile.

“You can think of any great place you love it’s probably not more than a block in size,” explains Dallas-based Jason Roberts, the father of the Better Block movement.

When people hear about good urban planning, Roberts says, they often shut down and concentrate on potential problems.

Bringing the concepts to life helps people make the connection and see the benefits, as opposed to the jams.

Three years ago, in Dallas, Roberts decided to stop talking and start doing. He and a small group [turned one abandoned block on a little-used eyesore of a street into a slice of utopia for a weekend](http://www.youtube.com/watch?v=hdZpJ5MwbqA).

“People experienced it and all those buzz words I’d been talking about—walkability, pedestrian friendly—all those things I’d been describing to people, they got it.”

Better Block BR’s organizers hope that’ll happen here too.

If the Better Block BR vision of Government Street becomes a reality, organizers say, it could transform Baton Rouge. It could also increase the bottom line for businesses that line Government. Right now, most people use the street as a way to get from point A to point B without a glance at places they could stop or shop.

“If we can get this done we know the rest of it will be easy and more of it will come,” says Samuel Sanders, executive director of the [Mid-City Redevelopment Alliance](http://www.midcityredevelopment.org/Pages/default.aspx).

Merchants such as Josh Holder, owner of [Time Warp Boutique](https://www.facebook.com/timewarpbr), say there’s a lot riding on Better Block BR. The vintage clothing retailer has spent the last 10 years being a destination business on Government. He’d love to have more stumbled-upon customers who are walking or biking the area and find him by accident.

“Right now I am looking at a Radio Shack, a Dollar Store and a Quick Cash,” he says of his block. “I could be looking at a really cool yogurt shop where the high school students can hang out or another boutique. We could be feeding off of each others’ businesses.”

[Radio Bar](https://www.facebook.com/theradiobar) is nearby, and Holder loves it. But its patrons often come to the area when his shop is closed. He’s thrilled that vintage vinyl and gifts store [Atomic Pop Shop](http://www.atomicpopshop.com/) is nearby, too. He’d like to see more growth in that direction.

If the pop-up district inspires people to start thinking about what types of shops will help give Government St. a character that jives with its neighborhoods, he says, it will be a win.

What went wrong with Government Street is pretty typical in cities, says Broome.

Like many streets in Baton Rouge, Government started out as a trail into the woods. Engineers developed it, then the people moved in. That’s a bit backwards if you want a city that really meets the needs of its citizens, Broome explains.

“The infrastructure has to be driven by the community. When there is not a comprehensive vision driven by the community, decisions are made on infrastructure.”

It’s not too late for people to get involved in shaping Baton Rouge’s streets. In February, more than 200 curious residents turned out to give Better Block BR’s organizers some input on what they’d like to see in the Better Block and to learn what it will take to bring out lasting change on a civic level. The Better Block BR Facebook page grows daily.

Better Block BR is taking place the same weekend as [Blues Fest](http://batonrougebluesfestival.org/), [Velo Louisiane](https://sites.google.com/a/brsafestreets.org/brass/events-1/untitledpost-3) and the [Green Crawl](http://www.greencrawlbr.com/).

No worries, says Phillip LaFargue, director of communications at CPEX. Organizers decided to launch Better Block BR on a weekend to make the function easier. But to grasp the vision for this part of the city, it’s vital that people use the road as they normally would.

Chances are, though, rubberneckers will want to stop once they see how inviting the infrastructure becomes when Better Block BR finishes bringing its ideas to life.

“Stop by and have a cup of coffee on the way to Blues Fest,” Broome says. “We want you to enjoy it just as you would Magazine Street.”