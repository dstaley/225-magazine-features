# 225 Features

As one of my first side projects for my job as the Digital Product Development Lead at 225 Magazine, I created a quick featured articles site that showcases the cool, interesting layouts that are possible with just a small amount of effort.

Needless to say that's as far as it went.

You can view a live version of the site [here](http://features.225batonrouge.com/). My favorite layouts is [this one](http://features.225batonrouge.com/thinking-for-a-change/).