---
title: Lolo Up and Running
subtitle: Lolo Jones returns to B.R. with a new foundation and an unexpected Olympic dream
author: Amy Alexander
image: "images/SCHNACK_LOLO-0223.jpg"
date:   2013-02-01 12:42:43
layout: post
custom_css: true
---
Each morning, says Lolo Jones, as that bright bulb rose over the Mississippi River, she felt a little kick in her consciousness that said, “Stretch. Run. Leap, 1-2-3. Leap, 1-2-3.”

Over and over again. Four hours a day, six days a week, at the LSU track and in the weight room, that’s how it went.

Though she traveled far and wide to meet sponsors and compete in critical meets, she always came back here to Baton Rouge for family and friends. For comfort and recovery.

It was called the disaster in Beijing.

Jones, by far the favorite for a 2008 Olympic gold medal in the women’s 100-meter hurdles, surged into the lead.

And then, just like that, fate shifted like a tectonic plate. Jones’s toe caught the edge of the second-to-last hurdle.

Dawn Harper, one of Jones’s teammates, took the gold. Jones came in seventh.

In the next 15 seconds, Jones assumed the position of one who is grieving.

Her gazelle’s legs crumpled painfully beneath her body, and she keened, pounding the brick-hued track.

The bright lights blazed at her balefully. Camera shutters clacked.

There was so much life in that moment. Potential. Agony. Defeat. Beauty.

For the next four years, then, Jones perched on the edge of America’s consciousness, as if permanently suspended in the Chinese air before that second-to-last hurdle.

The Des Moines, Iowa, native and LSU graduate came back Baton Rouge in 2008 to recover, reboot and as always, train.

<figure>
  <img src="{{ site.base_url }}images/SCHNACK_LOLO-0053.jpg">
  <figcaption>Jones feels at home in Baton Rouge, and says she never realized just how warm and friendly the city is until she traveled the world.</figcaption>
</figure>

She still had her sense of humor—“I haven’t eaten Chinese food since,” she quips.

But she didn’t forget. For four more years, sparked by that impossible second-to-last hurdle, Jones got up and kept going strong.

## Back to kindness

The 2012 London Olympics were supposed to bring redemption.

Jones, 30, wound up in fourth place—which was outstanding, really, considering her age, the talent-packed competition and the fact that she’d had spinal cord surgery in 2011.

Maybe because America is drawn to a stumbling tale more than a victory dance, we are still riveted by her. Jones has remained a fan favorite with a huge Twitter following. She’s someone in whom people still find plenty of potential.

<figure>
  <img src="{{ site.base_url }}images/Lolo Jones2_BY ERROL ANDERSON.jpg">
  <figcaption>At age 30, Jones turned heads again at the London Olympics last summer, but finished fourth in the 100-meter hurdles, missing the medal stand by one-tenth of a second.</figcaption>
</figure>

She came back to Baton Rouge again in the fall of 2012 to recover from the London Games and trace a new path forward.

From here, it seems, anything is possible.

This winter, she joined the U.S. national women’s bobsled team. She’ll hunt for gold in the 2014 Winter Olympics in Sochi, Russia. She plans to make another go with hurdling at the Summer Games in Rio in 2016.

<q class="left">"I’ve been doing the same thing for 13 years, and I thought it was time to do something else." <small>LOLO JONES</small></q>

Jones describes Baton Rouge as a city of kindness. Here, crawfish, Tony Chachere’s and triple-digit heat meld with family—many of Jones’ siblings followed her here—to make a place of recovery.

“The people here are just really welcoming and friendly,” she tells 225. “You don’t realize how much they are ’til you travel to other places. I like the feel of the city.”

Soon after she returned from the 2012 Games, Jones launched the Lolo Jones Foundation, which will give hope and resources to children who are facing their own hurdles in life.

“I hope my past experiences can help somebody out there,” she says.

## A brutal start

Growing up was painful for the Jones kids.

“We got through it by the grace of God,” agrees Angelia Jefferson, Jones’s older sister. “We got through it, and it makes you who you are. You grow in seasons of darkness and adversity.”

As a child, Jones often wondered where her next meal would come from. Her dad was in jail, and her mom, Lori, was just trying to get by while raising five kids.

They even spent time living in the murky basement of the Des Moines Salvation Army.

“There were years when my Christmas presents came from different charities,” she says.

Little Lolo Jones escaped into Once Upon a Time.

“She would sit in her room and read and read and read,” Jefferson says. “My brothers would be running around acting like fools, and she was just in her room reading.”

It wasn’t obvious that Jones was born with fleet feet. Few who looked at her as a kid imagined she’d end up an Olympic athlete. But she had one Olympic quality that shined.

“What seemed apparent to me was that my sister was very disciplined in everything she took on,” Jefferson says. “I’ve never seen a child as driven.”

By the time Jones got to Roosevelt High, she was winning track meets. She was lean and fast. And she could leap.

Her goals were also ambitious. There was never any such thing as good enough—not for Lolo.

In her senior year of high school in 2000, as she gripped three medals at the Iowa Class 3-A Girls State Track Meet, she was still restless.

“I don’t care about winning. I’d rather break a record,” she told Des Moines Register reporter John Naughton. “Quality is better than quantity, always.”

Back home, things were still unstable. The Jones family moved eight times in as many years. Eventually Lolo stayed with friends so she could graduate from Roosevelt High. 

Looking back, she says, she was afraid the bounty of schools that were offering her full scholarships wouldn’t be able to find her.

She worked at a local deli to help make ends meet. She ran the track in borrowed shoes.

“Lolo is absolutely a hometown favorite,” says Andrew Logue, a sportswriter at the Des Moines Register.

LSU managed to find Jones. She had star quality, and the powerhouse Tigers track program wanted her.

“[Lolo] made it very clear she was looking for a place where she could earn a business degree and work daily to achieve the goal of becoming an NCAA Champion and Olympic Athlete,” says LSU track and field coach Dennis Shaver. “I was very impressed with her passion and commitment coming from a 17-year-old.”

At LSU, Jones excelled on the track and in the classroom while logging many hours of community service, Shaver recalls. She had a choice to make on graduation day in 2005. She could step, armed with degrees in economics and Spanish, into the world of business, or she could spend her days running. Running and running.

Jones knew which way to go.

As far as she runs, though, her past informs every action she takes.

<figure>
  <img src="{{ site.base_url }}images/SCHNACK_LOLO-0188.jpg">
  <figcaption>If she works as hard on her charity as she does in the weight room, the Lolo Jones Foundation will help many single mothers, families of incarcerated loved ones, poverty stricken communities and youth in the area.</figcaption>
</figure>

Her foundation, as it gains speed, has the potential to become another Lance Armstrong Foundation, thriving because Jones has both name recognition and a great story that relates to her cause.

When it comes to philanthropy, Jones seems like a natural. She still goes back to Roosevelt High to build the track program there through donations of money and shoes. She’s always putting more than enough presents under the family Christmas tree, her sister, Angelia Jefferson, notes. “She’s a very giving person.”

In 2008, she was named the VISA Humanitarian Athlete of the Year.

The Lolo Jones Foundation will help single mothers, families of incarcerated loved ones, poverty stricken communities and youth.

In the Capital City, this significant non-athletic pursuit might just earn her more satisfaction than any medal ever could.

## Down time

Christmas lights are hung all around town.

Jones and Jefferson are trying to decide what the family should eat on Christmas Day.

“She likes to try new recipes,” Jefferson says. “She’s very adventurous like that.”

When she’s back in Baton Rouge, Jones likes to stay home. She’s got a long-legged, smoky-gray Weimaraner hound named Boudreaux.

This year, she counts her vacation time in a precious handful of days. It’s full-on bobsled season, and Jones has just come from France. This weekend, she’ll jet to Park City, Utah. But she’ll be back to the bayou for Christmas Eve, and she’ll probably go to one of the services at Healing Place Church, where she and her family are longtime members.

It’s good to be home.

## Critics and fans

The 2012 Olympic Games were exciting and cruel for Jones this time around. The road there was pocked with struggle. Injuries and spinal cord surgery made it seem like she wouldn’t make it.

She was still a media and sponsorship darling, though, and she went along with that part of any Olympic athlete’s career.

Oakley, Red Bull and Asics are just a few of the companies that support her. She appeared on the cover of Outside magazine.

But all the attention took a bitter turn for Jones in London.

Two brutal days before her final run, a New York Times columnist—ironically, a fellow LSU alum—became Jones’ most vocal critic. Who is this woman? And why does she deserve all these sponsors, even though she has yet to turn in Olympic gold?

Jones was devastated by the piece.

On NBC, her eyes filled with tears as she described what it felt like to be ripped apart by the media after giving your soul to represent your nation in the Games.

Does she wish, now, that she had responded to the columnist’s interview request so she could defend herself?

“He called my agent the day before it ran and asked for an interview,” she says. “We weren’t even doing interviews for NBC a week before. Why would I do an interview at the last minute? My job is to focus on the Olympics, and that is what I was doing.”

Back in the States, fans rushed in. We love you, Lolo, they said. Jones is healing, slowly. Focusing on the next goal helps.

There is still one thing missing on the wall in her Baton Rouge home.

That space is big enough to move Jones to the mountain. As a brakeman on the bobsled team, Jones will be part of the four-person squad that gives the sled more miles-per-hour as it careens down the ice.

This is refreshing, Jones says. “Track is an individual sport. The last time I had a team environment was when I was on a track team at LSU.”

The gorgeous architecture of her well-known face will be ensconced in a heavy helmet. Her 12-pack abs hidden by high-tech fabric. But her heart will burn for that medal, hot as the roof of the Shaw Center in July.

“If you ask every athlete at the Olympics, they are all chasing a medal,” she says. “Their dreams aren’t just to be an Olympic athlete. It’s to be an Olympic medalist.” 